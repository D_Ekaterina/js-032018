let idCount = 1;

//Вычитание единицы
function minusOne(argument) {

    if (argument > 0 && argument <= 10) {
        argument --;
    }
    else {
        argument = 10;
    }

    return argument;
}

//Прибавления единицы
function plusOne(argument) {

    if (argument >= 0 && argument < 10) {
        argument ++;
    }
    else {
        argument = 0;
    }

    return argument;
}

// Создание новых счетчиков
function createCount (argument) {
    let place = document.getElementsByClassName('counters')[0];
    let count1 = document.createElement('input');

    count1.className = 'counts';
    count1.setAttribute('id', 'decreaseByOne' + argument);
    count1.setAttribute('type', 'button');
    count1.setAttribute('value', 'Уменьшить на -1');

    let count2 = document.createElement('input');
    count2.setAttribute('id', 'increaseByOne' + argument);
    count2.className = 'counts';
    count2.setAttribute('type', 'button');
    count2.setAttribute('value', 'Увеличить на +1');

    let result1 = document.createElement('div');
    result1.setAttribute('id', 'number' + argument);
    result1.className = 'number';
    result1.innerHTML = 0;

    place.appendChild(count1);
    place.appendChild(count2);
    place.appendChild(result1);
}

//Вычисление значения для новых счетчиков
function calculatingValue(argument) {

// Вычитание единицы и вывод значения в div при нажатии на кнопку для новых счетчиков
document.getElementById('decreaseByOne' + argument).addEventListener('click', function() {
    document.getElementById('number' + argument).innerHTML 
    = minusOne(+document.getElementById('number' + argument).innerHTML);
});

// Прибавление единицы и вывод значения в div при нажатии на кнопку для новых счетчиков
document.getElementById('increaseByOne' + argument).addEventListener('click', function() {
    document.getElementById('number' + argument).innerHTML 
    = plusOne(+document.getElementById('number' + argument).innerHTML);
});
}

// Вычитание единицы и вывод значения в div при нажатии на кнопку
document.getElementById('decreaseByOne').addEventListener('click', function() {
    document.getElementById('number').innerHTML 
    = minusOne(+document.getElementById('number').innerHTML);
});

// Прибавление единицы и вывод значения в div при нажатии на кнопку
document.getElementById('increaseByOne').addEventListener('click', function() {
    document.getElementById('number').innerHTML 
    = plusOne(+document.getElementById('number').innerHTML);
});

//Прибавление и вычитание 1 при нажатии стрелок на клавиатуре
addEventListener("keydown", function(event) {

    if (event.keyCode === 39) {
        document.getElementById('number').innerHTML 
        = plusOne(+document.getElementById('number').innerHTML);
    }

    if (event.keyCode === 37) {
        document.getElementById('number').innerHTML 
        = minusOne(+document.getElementById('number').innerHTML);
    }
});

document.getElementById('createCount').addEventListener('click', function() {
    createCount(idCount);
    calculatingValue(idCount);
    idCount = idCount + 1;
});
