let count = 0;

//Вычитание единицы
function minusOne(argument) {

    if (argument > 0 && argument <= 10) {
        argument --;
    }
    else {
        argument = 10;
    }

    count = argument;

    return count;
}

//Прибавления единицы
function plusOne(argument) {

    if (argument >= 0 && argument < 10) {
        argument ++;
    }
    else {
        argument = 0;
    }

    count = argument;

    return count;
}

// Вычитание единицы и вывод значения в div при нажатии на кнопку
document.getElementById('decreaseByOne').addEventListener('click', function() {
    document.getElementById('number').innerHTML = minusOne(count);
});

// Прибавление единицы и вывод значения в div при нажатии на кнопку
document.getElementById('increaseByOne').addEventListener('click', function() {
    document.getElementById('number').innerHTML = plusOne(count);
});

//Прибавление и вычитание 1 при нажатии стрелок на клавиатуре
addEventListener("keydown", function(event) {

    if (event.keyCode === 39) {
        document.getElementById('number').innerHTML = plusOne(count);
    }

    if (event.keyCode === 37) {
        document.getElementById('number').innerHTML = minusOne(count);
    }
});
