//Получаем значения, преобразовываем в массив чисел
function getNumber() {
    const inputNumbers = document.getElementById('number1').value;
    let arrayValue = inputNumbers.split(',');

    for (let i = 0; i < arrayValue.length; i++) {
        arrayValue[i] = +arrayValue[i];
    }
    
    return arrayValue;
}

// Сортировка массива
function sortArray(arrayNumber) {

    for (let i = 0; i < arrayNumber.length; i++) {

        for (let j = 0; j < arrayNumber.length; j++) {

            if (arrayNumber[i] < arrayNumber[j]) {
                let timeVar = arrayNumber[i];
                arrayNumber[i] = arrayNumber[j];
                arrayNumber[j] = timeVar;
            }
        }
    }

    return arrayNumber;
}

//Поиск медианы
function searchMedian(sortNumber) {
    let result;

    if (sortNumber.length % 2 === 0) {
        result = (sortNumber[sortNumber.length / 2 - 1] 
        + sortNumber[sortNumber.length / 2]) / 2;
    }

    if (sortNumber.length % 2 !== 0) {
        let timeVar = Math.floor(sortNumber.length / 2);
        result = sortNumber[timeVar];
    }
    
    return result;
}

//Поиск медианы и вывод на экран по клику
startCount.onclick = () => {
    let result = searchMedian(sortArray(getNumber()));
    document.getElementById('result').innerHTML = result;
};