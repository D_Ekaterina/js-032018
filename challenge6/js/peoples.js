// Глобальные переменные
const startPeopleCount = 5;
let addPeopleCount = startPeopleCount + 1;
let peoples = {};
let countForNewBlock = 1;

// Функция генерации цвета
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
}

// Функция генерации имения
function generateName() {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
}

// Создание массива человечков и сохранение их в глобальной переменной
let createPeoples = (peopleCount) => {
    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor(),
            block: 0
        }
    }
};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
// TODO: возможно стоит поменять название на более подходящее
let displayPeoples = (peoplesForDisplay) => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];

    for (let people in peoplesForDisplay) {
        let peopleElement = document.createElement('i');

        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesForDisplay[people].id);
        peopleElement.setAttribute('data-name', peoplesForDisplay[people].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesForDisplay[people].color);

        wrapperPeoples.appendChild(peopleElement);

        peoples[people].domElement = peopleElement;
    }
};
// Функция, которая срабатывает при клике на человечка
let clickedByPeople = () => {
    let elements = document.querySelectorAll(".fa-male");

    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", function() {
             elements[i].style.color = getRandomColor();
        });
    }
};

let addPeoples = (addPeopleCount) => {
    peoples[addPeopleCount] = {
        id: addPeopleCount,
        name: generateName(),
        color: getRandomColor(),
        block: 0
    }
     console.log(peoples);
};
let conclusionPeoples = (addPeopleCount) => {

    let place = document.getElementsByClassName('peoples-container')[0];
    
    let addPeople = document.createElement('i');

    addPeople.className = 'fas fa-male';
    addPeople.setAttribute('data-id', addPeopleCount);
    addPeople.setAttribute('data-name', generateName());
    addPeople.setAttribute('style', 'color: ' + getRandomColor());

    place.appendChild(addPeople);
    peoples[addPeopleCount].domElement = addPeople;

    addPeople.addEventListener("click", function() {
        addPeople.style.color = getRandomColor();
    });
};

let addPeoplesByClick = () => {
    addPeoples(addPeopleCount);
    conclusionPeoples(addPeopleCount);
    addPeopleCount = addPeopleCount + 1;
};

let deletePeople = () => {
    if (addPeopleCount > 2) {
        delete peoples[addPeopleCount-1];
        addPeopleCount = addPeopleCount-1;
        let place = document.getElementsByClassName('peoples-container')[0];
        place.removeChild(place.lastChild);
    }
    else {
        alert('Вы не можете удалить всех человечков!')
    }
};

let createBlockForPeople = () => {
    let place = document.getElementsByClassName('fieldForBlock')[0];
    let addPeople = document.createElement('div');
    let addSubmitForAdd = document.createElement('input');
    let addSubmitForDelete = document.createElement('input');

    addPeople.className = 'peoples-container' + countForNewBlock;
    addSubmitForAdd.setAttribute('data-id', 'birthPeople' + countForNewBlock);
    addSubmitForAdd.setAttribute('type', 'button');
    addSubmitForAdd.setAttribute('value', 'Создать человечка');

   // addSubmitForDelete.setAttribute('data-id', 'deadPeople' + countForNewBlock);
   // addSubmitForDelete.setAttribute('type', 'button');
   // addSubmitForDelete.setAttribute('value', 'Удалить человечка');

     place.appendChild(addPeople);
     place.appendChild(addSubmitForAdd);
    // place.appendChild(addSubmitForDelete);
     addSubmitForAdd.addEventListener("click", addPeoplesByClickBlock);
    // addSubmitForDelete.addEventListener("click", addPeoplesByClick);

     countForNewBlock = countForNewBlock + 1;
};

let counteres = () => {
    let varTemp = countForNewBlock - 1;
    return varTemp;
};

let addPeoplesForBlock = (addPeopleCount, argument) => {
    peoples[addPeopleCount] = {
        id: addPeopleCount,
        name: generateName(),
        color: getRandomColor(),
        block: argument
    }
    console.log(peoples);
};

let addPeoplesByClickBlock = () => {
    addPeoplesForBlock(addPeopleCount, counteres());
    conclusionPeoplesForBlock(addPeopleCount, counteres());
    addPeopleCount = addPeopleCount + 1;
};

let conclusionPeoplesForBlock = (addPeopleCount, argument) => {
    let place = document.getElementsByClassName('peoples-container' + argument)[0];
    let addPeople = document.createElement('i');

    addPeople.className = 'fas fa-male';
    addPeople.setAttribute('data-id', addPeopleCount);
    addPeople.setAttribute('data-name', generateName());
    addPeople.setAttribute('style', 'color: ' + getRandomColor());

    place.appendChild(addPeople);
    peoples[addPeopleCount].domElement = addPeople;

    addPeople.addEventListener("click", function() {
        addPeople.style.color = getRandomColor();
    });
};
// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
let domContentLoaded = () => {
    createPeoples(startPeopleCount);
    displayPeoples(peoples);
    document.getElementById("birthPeople").addEventListener("click", addPeoplesByClick);
    clickedByPeople();
    document.getElementById("deadPeople").addEventListener("click", deletePeople);
    document.addEventListener("contextmenu", deletePeople);
    document.getElementById("createNewBlock").addEventListener("click", createBlockForPeople);

};
document.addEventListener('DOMContentLoaded', domContentLoaded);

