const gulp = require('gulp');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const watch = require('gulp-watch');

// Сжимаем наши js файлы
// Переводим их в старую версию js
// Кладём в папку js_compiled
gulp.task('js', () => {
    gulp.src('./js/*.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./js_compiled/'))
});

// Кладём jquery в папку js_compiled
gulp.task('jquery', () => {
    return gulp.src('./node_modules/jquery/dist/jquery.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js_compiled/'))
});

// Дефолтная задача
gulp.task('default', ['js', 'jquery']);

// Вотчер за нашими js файлами
gulp.task('watch', () => {
    gulp.watch(['./js/*.js'], ['js']);
});