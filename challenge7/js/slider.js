const sliderState = {
    activeSlide: 0,
    allSlides: {},
    slidesCount: 0
};

$(document).ready(() => {
    sliderInit();
    $('#slider img').on('click', clickSlide);
});

const sliderInit = () => {
    const slides = $('#slider img');

    sliderState.slidesCount = slides.length;
    sliderState.allSlides = slides.map((index, element) => {
        return {
            id: index,
            tag: $(element)
        }
    });
    displayActiveSlide(sliderState.allSlides, sliderState.activeSlide);
};

const displayActiveSlide = (sliders, numberActiveSlider) => {

    for (let i = 0; i < sliders.length; i++) {
        if (i === numberActiveSlider) {
            sliders[i].tag.fadeIn(300);
        }
        else {
            sliders[i].tag.hide();
        }
    }

    return numberActiveSlider;
};

const clickSlide = () => {
    sliderState.activeSlide = nextSlideFunc(sliderState.activeSlide, sliderState.slidesCount);
    displayActiveSlide(sliderState.allSlides, sliderState.activeSlide);
};

const clickSlidePrev = () => {
    sliderState.activeSlide = prevSlideFunc(sliderState.activeSlide, sliderState.slidesCount);
    displayActiveSlide(sliderState.allSlides, sliderState.activeSlide);
};

const nextSlideFunc = (currentSlide, maxSlide) => {
    let nextSlide = ++currentSlide;

    if (nextSlide >= maxSlide) {
        nextSlide = 0;
    }

    return nextSlide;
};

const prevSlideFunc = (currentSlide, maxSlide) => {
    let prevSlide = --currentSlide;

    if (prevSlide < 0) {
        prevSlide = maxSlide;
    }

    return prevSlide;
};

$('#backward').on('click', clickSlidePrev);
$('#forward').on('click', clickSlide);