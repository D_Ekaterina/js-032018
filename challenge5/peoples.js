// Глобальные переменные
const startPeopleCount = 5;
let addPeopleCount = startPeopleCount + 1;
let peoples = {};
let countForNewBlock = 1;

// Функция генерации цвета
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
}

// Функция генерации имения
function generateName() {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++) {
        name += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return name;
}

// Создание массива человечков и сохранение их в глобальной переменной
let createPeoples = (peopleCount) => {
    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor(),
        }
    }
};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
// TODO: возможно стоит поменять название на более подходящее
let displayPeoples = (peoplesForDisplay) => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];

    for (let people in peoplesForDisplay) {
        let peopleElement = document.createElement('i');

        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesForDisplay[people].id);
        peopleElement.setAttribute('data-name', peoplesForDisplay[people].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesForDisplay[people].color);

        wrapperPeoples.appendChild(peopleElement);

        peoples[people].domElement = peopleElement;
    }
};

// Функция, которая срабатывает при клике на человечка
let clickedByPeople = () => {
    let elements = document.querySelectorAll(".fa-male");

    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", function() {
             elements[i].style.color = getRandomColor();
        });
    }
};

let addPeoples = (addPeopleCount) => {
    peoples[addPeopleCount] = {
        id: addPeopleCount,
        name: generateName(),
        color: getRandomColor()
    }

    console.log(peoples);
};

let conclusionPeoples = (addPeopleCount) => {
    let place = document.getElementsByClassName('peoples-container')[0];
    let addPeople = document.createElement('i');

    addPeople.className = 'fas fa-male';
    addPeople.setAttribute('data-id', addPeopleCount);
    addPeople.setAttribute('data-name', generateName());
    addPeople.setAttribute('style', 'color: ' + getRandomColor());

    place.appendChild(addPeople);
    peoples[addPeopleCount].domElement = addPeople;

    addPeople.addEventListener("click", function() {
        addPeople.style.color = getRandomColor();
    });
};

let addPeoplesByClick = () => {
    addPeoples(addPeopleCount);
    conclusionPeoples(addPeopleCount);
    addPeopleCount = addPeopleCount + 1;
};

let deletePeople = () => {
    if (addPeopleCount > 2) {
        delete peoples[addPeopleCount-1];
        addPeopleCount = addPeopleCount-1;
        let place = document.getElementsByClassName('peoples-container')[0];
        place.removeChild(place.lastChild);
    }
    else {
        alert('Вы не можете удалить всех человечков!')
    }
};

// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
let domContentLoaded = () => {
    createPeoples(startPeopleCount);
    displayPeoples(peoples);
    document.getElementById("birthPeople").addEventListener("click", addPeoplesByClick);
    clickedByPeople();
    document.getElementById("deadPeople").addEventListener("click", deletePeople);
    document.addEventListener("contextmenu", deletePeople);
};
document.addEventListener('DOMContentLoaded', domContentLoaded);

