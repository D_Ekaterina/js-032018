
// Получаем число, которое ввел пользоватьль
function getNumber() {
    const inputNumber = document.getElementById('inputNumber').value;
    let numberForCalculate = +inputNumber;

    return numberForCalculate;
}

//Вычитание единицы и вывод значения в input
function minusOne(argument) {

    if (argument > 0) {
        argument --;
    }

    return argument;
}

//Прибавления единицы и вывод значения в input
function plusOne(argument) {

    if (argument >= 0) {
        argument ++;
    }

    return argument;
}

// Вычитание единицы и вывод значения в input при нажатии на кнопку
document.getElementById('decreaseByOne').addEventListener('click', function() { 
    document.getElementById('inputNumber').value = minusOne(getNumber()); 
});

// Прибавление единицы и вывод значения в input при нажатии на кнопку
document.getElementById('increaseByOne').addEventListener('click', function() { 
    document.getElementById('inputNumber').value = plusOne(getNumber()); 
});
