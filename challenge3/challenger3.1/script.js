let count = 0;

//Вычитание единицы
function minusOne(argument) {

    if (argument > 0) {
        argument --;
    }
    count = argument;

    return count;
}

//Прибавления единицы
function plusOne(argument) {

    if (argument >= 0) {
    argument ++;
}

    count = argument;

    return count;
}

// Вычитание единицы и вывод значения в div при нажатии на кнопку
document.getElementById('decreaseByOne').addEventListener('click', function() {
    document.getElementById('number').innerHTML
    = minusOne(count);
});

// Прибавление единицы и вывод значения в div при нажатии на кнопку
document.getElementById('increaseByOne').addEventListener('click', function() {
    document.getElementById('number').innerHTML
    = plusOne(count);
});
